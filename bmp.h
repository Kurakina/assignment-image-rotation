#ifndef BMP_H_
#define BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <memory.h>
#include "picture.h"

#define COLOR_DEPTH 24
#define PADDING_SIZE 4
#define BMP_TYPE 0x4d42

#pragma pack(push, 1)
struct BmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint32_t getPadding(const uint64_t width);

enum ReadStatus {
    READ_CHECK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE,
    READ_UNSUPPORTED_DEPTH
};

enum ReadStatus fromBmp(FILE* in, struct Picture* pic);

enum WriteStatus {
    WRITE_CHECK = 0,
    WRITE_ERROR,
    WRITE_INVALID_BITS
};

enum WriteStatus toBmp(FILE* out, struct Picture const* pic);

#endif
