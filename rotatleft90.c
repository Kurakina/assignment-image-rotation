#include "rotateleft90.h"

struct Picture rotateLeft90(struct Picture const source) {
    struct Picture newPicture;

    newPicture.h = source.w;
    newPicture.w = source.h;
    newPicture.data = malloc(source.w * source.h * sizeof(struct Pixel));

    for (size_t y = 0; y < source.h; ++y) {
        for (size_t x = 0; x < source.w; ++x) {
            *(newPicture.data + x * newPicture.w + newPicture.w - y - 1) = *(source.data + y * source.w + x);
        }
    }

    return newPicture;
}