#ifndef PICTURE_H_
#define PICTURE_H_

#include <stdint.h>

struct Pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct Picture {
    uint64_t h;
    uint64_t w;
    struct Pixel* data;
};

#endif
